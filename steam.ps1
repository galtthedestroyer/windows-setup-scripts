#1 on a BAH machine you must right click powershell and run as powerbroker administrator to run this script.  
# To do so you'll probably have to pin powershell to the start menu then drag a shortcut to the desktop.  The shortcut is right-clickable.

#2 You probably will have to manually make this script runnable by executing the following command in powershell:
# Set-ExectionPolicy Unrestricted
# or if windows says there is no such command run it without the parameter then type the parameter after it asks.

#3 install chocolatey for the rest of this script to work.  https://chocolatey.org/install#install-with-powershellexe
Set-ExecutionPolicy Bypass -Scope Process -Force; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))

#5 install programs
#a. if you need a newer version but the choco package is still in moderation then specify the version on the command line
#all install / init commands are idempotent (it won't reinstall)
#b. https://status.chocolatey.org/

choco upgrade -y git

#keep git from clobbering line endings
git config --global core.autocrlf false

git clone https://github.com/Sycnex/Windows10Debloater.git
cd windows10debloater
./Windows10SysPrepDebloater.ps1 -Sysprep -Debloat -Privacy

choco upgrade -y 7zip.install
choco upgrade -y aria2
choco upgrade -y openssh
choco upgrade -y openssl
#choco upgrade firefox -y
choco upgrade -y googlechrome
choco upgrade -y steam-client
choco upgrade -y vim

#refreshenv is supposed to make this shell instance aware of the newly installed programs.  it doesn't.  open a new powershell.  
refreshenv

